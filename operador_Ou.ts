//testando o operador_Ou //

namespace operador_Ou
{
    let idade = 10; 
    let maiorIdade = idade > 18; 
    let possuiAutorizacaoDosPais = false; 
    
    let podeBeber = maiorIdade || possuiAutorizacaoDosPais; 
    console.log(podeBeber); // true
}